<?php


namespace Service;


use Ad\Model\Ad;
use Ad\Provider;
use Entity\AdEntity;

/**
 * Ad service in business layer using unit of work pattern for entity persisting.
 *
 */
class AdService
{
    private $entityManager;

    public function load(Provider $provider) {
        $entityManager = $this->entityManager;
        array_walk($provider->provide(), function(Ad $ad) use($entityManager) {

            $entityManager->create($this->createEntity($ad));
        });

        $entityManager->flush();
    }

    public function loadAll(array $providers) {
        array_walk($providers, [$this, 'load']);
    }

    protected function createEntity(Ad $ad): AdEntity {
        //Groups, Keywords and Campaigns need to be searched in storage before creating new records.

        return null;
    }
}