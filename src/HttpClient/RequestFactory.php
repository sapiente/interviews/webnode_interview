<?php


namespace HttpClient;
use Psr\Http\Message\RequestInterface;

/**
 * Factory method pattern defines interface for creating object,
 * but let subclasses to decide which class to instantiate.
 */
interface RequestFactory
{
    public function createRequest(): RequestInterface;
}