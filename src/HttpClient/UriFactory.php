<?php


namespace HttpClient;


use Psr\Http\Message\UriInterface;

/**
 * Factory method pattern defines interface for creating object,
 * but let subclasses to decide which class to instantiate.
 */
interface UriFactory
{
    public function createUri(): UriInterface;
}