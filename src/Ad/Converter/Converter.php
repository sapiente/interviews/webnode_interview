<?php


namespace Ad\Converter;


use Ad\Container\Container;

interface Converter
{
    /**
     * @param string $content
     * @return Container with data
     */
    public function convert(string $content): Container;
}