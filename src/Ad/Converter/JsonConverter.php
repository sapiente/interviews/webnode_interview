<?php


namespace Ad\Converter;


use Ad\Container\ArrayContainer;
use Ad\Container\Container;

class JsonConverter implements Converter
{
    /**
     * @inheritDoc
     */
    public function convert(string $content): Container
    {
        return $this->map(json_decode($content, true));
    }

    protected function map(array $data): ArrayContainer
    {
        foreach ($data as $key => $value) {
            if(is_array($data)) {
                $data[$key] = $this->map($data[$key]);
            }
        }

        return new ArrayContainer($data);
    }
}