<?php


namespace Ad\Container;


interface Container extends \IteratorAggregate
{
    /**
     * @param string $name attribute name using dot notation
     * @return mixed Scalar value or nested Container
     * @throws \InvalidArgumentException
     */
    public function getOrDie(string $name);

    /**
     * @param string $name attribute name using dot notation
     * @param mixed $default value if attribute is not found
     * @return mixed Scalar value or nested Container
     */
    public function getOrDefault(string $name, $default);

    /**
     * @param string $name attribute name using dot notation
     * @return bool True if source has attribute, false otherwise.
     */
    public function has(string $name): bool;

    /**
     * @param bool $convertNested If true, nested containers will be converted to array too
     * @return array associative array with container data
     */
    public function toArray(bool $convertNested): array;
}