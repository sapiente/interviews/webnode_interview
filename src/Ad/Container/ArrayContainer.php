<?php


namespace Ad\Container;


class ArrayContainer implements Container
{
    /** @var array */
    protected $data;

    /**
     * ArrayContainer constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @inheritDoc
     */
    public function getOrDie(string $name)
    {
        $data = $this->data;
        $segments = explode('.', $name);
        while(! empty($segments)) {
            $segment = array_shift($segments);

            if($data instanceof Container) {
                return $data->getOrDie(implode(".", $segment));
            }

            if(! key_exists($segment, $data)) {
                throw new \RuntimeException();
            }

            $data = $data[$segment];
        }

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function getOrDefault(string $name, $default)
    {
        $data = $this->data;
        $segments = explode('.', $name);
        while(! empty($segments)) {
            $segment = array_shift($segments);

            if($data instanceof Container) {
                return $data->getOrDefault(implode(".", $segment), $default);
            }

            if(! key_exists($segment, $data)) {
                return $default;
            }

            $data = $data[$segment];
        }

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function has(string $name): bool
    {
        $data = $this->data;
        $segments = explode('.', $name);
        while(! empty($segments)) {
            $segment = array_shift($segments);

            if($data instanceof Container) {
                return $data->has(implode(".", $segment));
            }

            if(! key_exists($segment, $data)) {
                return false;
            }

            $data = $data[$segment];
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function toArray(bool $convertNested): array
    {
        return (! $convertNested) ? $this->data : array_map(function($item) {
                    return $item instanceof Container ? $item->toArray(true) : $item;
                }, $this->data);
    }

    /**
     * @inheritDoc
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->data);
    }
}