<?php


namespace Ad\Model;

/**
 * DTO pattern (POPO with public fields)
 */
class Ad
{
    /** @var \DateTime */
    public $date;
    /** @var double */
    public $price;
    /** @var int */
    public $views;
    /** @var int */
    public $clicks;
    /** @var int */
    public $buys;
    /** @var AdGroup */
    public $group;
    /** @var AdCampaign */
    public $campaign;
}