<?php


namespace Ad\Model;

/**
 * DTO pattern (POPO with public fields)
 */
class AdGroup
{
    /** @var int */
    public $id;
    /** @var Keyword[] */
    public $keywords;
}