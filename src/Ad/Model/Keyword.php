<?php


namespace Ad\Model;

/**
 * DTO pattern (POPO with public fields)
 */
class Keyword
{
    /** @var string */
    public $value;
}