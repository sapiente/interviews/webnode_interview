<?php


namespace Ad\Provider\Bing;


use Ad\Container\Container;
use Ad\Model\Ad;
use Ad\Model\AdGroup;
use Ad\Model\Keyword;
use Ad\Provider\Mapper as IMapper;

class Mapper implements IMapper
{
    public function map(Container $container): Ad
    {
        $ad = new Ad();

        $ad->buys = $container->getOrDie("buys");
        $ad->group = $this->mapGroup($container);
        $ad->date = new \DateTime($container->getOrDie("date"));

        return $ad;
    }

    protected function mapGroup(Container $container) {
        $group = new AdGroup();
        $group->id = $container->getOrDie("group.id");
        $group->keywords = array_map([$this, 'mapKeyword'], $container->getOrDefault('group.keywords', []));

        return $group;
    }

    protected function mapKeyword(Container $container): Keyword {
        $keyword = new Keyword();
        $keyword->value = $container->getOrDie("value");

        return $keyword;
    }
}