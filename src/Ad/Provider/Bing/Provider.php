<?php


namespace Ad\Provider\Bing;

use Ad\Provider\Mapper;
use Ad\Provider\HttpProvider;;
use HttpClient\RequestFactory;
use HttpClient\UriFactory;
use Psr\Http\Message\RequestInterface;

class Provider extends HttpProvider
{
    protected function getRequest(RequestFactory $requestFactory, UriFactory $uriFactory): RequestInterface
    {
        $request = $requestFactory->createRequest();
        $uri = $uriFactory->createUri();
        $uri->withHost("example.com")->withPath("api/ad");

        return $request->withUri($uri)->withMethod("GET");
    }

    protected function getMapper(): Mapper
    {
        return new \Ad\Provider\Bing\Mapper();
    }
}