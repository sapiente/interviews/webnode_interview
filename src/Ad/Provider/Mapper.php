<?php


namespace Ad\Provider;


use Ad\Container\Container;
use Ad\Model\Ad;

interface Mapper
{
    public function map(Container $container): Ad;
}