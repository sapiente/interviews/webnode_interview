<?php


namespace Ad\Provider;


use Ad\Converter\Converter;
use Ad\Provider;
use HttpClient\Client;
use HttpClient\RequestFactory;
use HttpClient\UriFactory;
use Psr\Http\Message\RequestInterface;

/**
 * Template method pattern defines the skeleton of an algorithm in an operation, deferring some steps to subclass.
 */
abstract class HttpProvider implements Provider
{
    /** @var Client */
    private $client;
    /** @var Converter */
    private $converter;
    /** @var RequestFactory */
    private $requestFactory;
    /** @var UriFactory */
    private $uriFactory;

    /**
     * Provider constructor.
     * @param Client $client
     * @param Converter $converter
     * @param RequestFactory $requestFactory
     * @param UriFactory $uriFactory
     */
    public function __construct(Client $client, Converter $converter, RequestFactory $requestFactory, UriFactory $uriFactory)
    {
        $this->client = $client;
        $this->converter = $converter;
        $this->requestFactory = $requestFactory;
        $this->uriFactory = $uriFactory;
    }

    /**
     * @inheritdoc
     */
    public function provide(): array
    {
        $response = $this->client->sendRequest($this->getRequest($this->requestFactory, $this->uriFactory));
        $data = $this->converter->convert($response->getBody()->__toString());

        return array_map([$this->getMapper(), 'map'], iterator_to_array($data));
    }

    protected abstract function getRequest(RequestFactory $requestFactory, UriFactory $uriFactory): RequestInterface;

    protected abstract function getMapper(): Mapper;
}