<?php


namespace Ad;


use Ad\Model\Ad;

interface Provider
{
    /**
     * @return Ad[]
     */
    public function provide() : array;
}