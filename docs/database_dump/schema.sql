CREATE TABLE ad_campaigns
(
  id INT AUTO_INCREMENT
    PRIMARY KEY
)
  ENGINE = InnoDB;

CREATE TABLE ad_groups
(
  id          INT AUTO_INCREMENT
    PRIMARY KEY,
  ad_campaign INT NULL,
  CONSTRAINT ad_groups_ad_campaigns_id_fk
  FOREIGN KEY (ad_campaign) REFERENCES ad_campaigns (id)
)
  ENGINE = InnoDB;

CREATE INDEX ad_groups_ad_campaigns_id_fk
  ON ad_groups (ad_campaign);

CREATE TABLE ads
(
  id       INT AUTO_INCREMENT
    PRIMARY KEY,
  ad_group INT      NULL,
  views    INT      NOT NULL,
  clicks   INT      NOT NULL,
  buys     INT      NOT NULL,
  date     DATETIME NOT NULL,
  price    DOUBLE   NOT NULL,
  CONSTRAINT ads_ad_groups_id_fk
  FOREIGN KEY (ad_group) REFERENCES ad_groups (id)
)
  ENGINE = InnoDB;

CREATE INDEX ads_ad_groups_id_fk
  ON ads (ad_group);

CREATE TABLE keywords
(
  id       INT AUTO_INCREMENT
    PRIMARY KEY,
  value    VARCHAR(255) NOT NULL,
  ad_group INT          NOT NULL,
  CONSTRAINT keywords_ad_groups_id_fk
  FOREIGN KEY (ad_group) REFERENCES ad_groups (id)
)
  ENGINE = InnoDB;

CREATE INDEX keywords_ad_groups_id_fk
  ON keywords (ad_group);

