# Get ad count by keyword
SET @`keyword` = 'display';
SELECT COUNT(ads.id) as ad_count FROM ads
  JOIN ad_groups ON ads.ad_group = ad_groups.id
  JOIN keywords k ON (ad_groups.id = k.ad_group AND k.value = @`keyword`);

# Get price sum by keyword, analogously for clicks, buys, views
SELECT SUM(price) as price_sum, k.value as keyword FROM ads
  JOIN ad_groups ON ads.ad_group = ad_groups.id
  JOIN keywords k ON (ad_groups.id = k.ad_group)
  GROUP BY k.id;

# get ad occurrence count in time flow
# SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY','')); - allows select columns which are not in group by
SET @`interval` = 60*60*24; #divide by days
SELECT COUNT(id) as `count`, TO_SECONDS(date) DIV @`interval` as `date` FROM ads
  GROUP BY TO_SECONDS(date) DIV @`interval`;